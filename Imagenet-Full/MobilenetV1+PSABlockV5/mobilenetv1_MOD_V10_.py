import torch
import torch.nn as nn

from PooledSpatialAttentionV5_Soft_Res_ import PooledSpatialAttentionBlockV5

class FlyOver(nn.Module):
	def __init__(self, inplanes, outplanes, h, w):
		super(FlyOver, self).__init__()
		
		def conv_stacked(inp, oup, stride):
			return nn.Sequential(
				nn.Conv2d(inp, inp, 1, stride, 0, bias=False),
				nn.BatchNorm2d(inp),
				nn.ReLU(inplace=True),
				nn.Conv2d(inp, inp, 1, stride, 0, bias=False),
				nn.BatchNorm2d(inp),
				nn.ReLU(inplace=True),
			)
		
		self.PSABV5_1 = PooledSpatialAttentionBlockV5(inplanes, 1, h, w)
		self.conv_stack_1 = conv_stacked(inplanes, inplanes, 1)
		self.PSABV5_2 = PooledSpatialAttentionBlockV5(inplanes, 1, h, w)	

	def forward(self, x):
		out = self.PSABV5_1(x)
		# print(out.shape)
		out = self.conv_stack_1(out)
		# print(out.shape)

		out = self.PSABV5_2(out)
		# print(out.shape, x.shape)
		out = out + x
		return out

class mobilenetv1(nn.Module):
	def __init__(self, num_classes):
		super(mobilenetv1, self).__init__()

		def conv_bn(inp, oup, stride):
			return nn.Sequential(
				nn.Conv2d(inp, oup, 3, stride, 1, bias=False),
				nn.BatchNorm2d(oup),
				nn.ReLU(inplace=True),
			)

		def conv_stacked(inp, oup, stride):
			return nn.Sequential(
				nn.Conv2d(inp, inp, 1, stride, 1, bias=False),
				nn.BatchNorm2d(inp),
				nn.ReLU(inplace=True),
				nn.Conv2d(inp, inp, 1, stride, 1, bias=False),
				nn.BatchNorm2d(inp),
				nn.ReLU(inplace=True),
			)
			
		def conv_dw(inp, oup, stride):
			return nn.Sequential(
				nn.Conv2d(inp, inp, 3, stride, 1, groups=inp, bias=False),
				nn.BatchNorm2d(inp),
				nn.ReLU(inplace=True),
	
				nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
				nn.BatchNorm2d(oup),
				nn.ReLU(inplace=True),
			)
		

		self.model = nn.Sequential(
			conv_bn(  3,  32, 2), 
			conv_dw( 32,  64, 1),
			conv_dw( 64, 128, 2),
			conv_dw(128, 128, 1),
			conv_dw(128, 256, 2),
			conv_dw(256, 256, 1),
			conv_dw(256, 512, 2),
			conv_dw(512, 512, 1),
			FlyOver(512, 512, 14, 14),
			conv_dw(512, 1024, 2),
			conv_dw(1024, 1024, 1),
			nn.AvgPool2d(7),
			
		)
		self.fc = nn.Linear(1024, num_classes)

	def forward(self, x):
		x = self.model(x)
		x = x.view(-1, 1024)
		x = self.fc(x)
		return x
		