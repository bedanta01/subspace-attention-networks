import torch 
import torch.nn as nn
import torch.nn.functional as F
from LinearBottleneck import LinearBottleneck 

from PooledSpatialAttentionV5_Soft_Res_ import PooledSpatialAttentionBlockV5

class MobileNetV2(nn.Module):
	def __init__(self, num_classes):
		super(MobileNetV2, self).__init__()
		
		self.t = [1, 6, 6, 6, 6, 6, 6]
		self.c = [32, 16, 24, 32, 64, 96, 160, 320]
		self.n = [1, 1, 2, 3, 4, 3, 3, 1]
		self.s = [2, 1, 2, 2, 2, 1, 2, 1]

		# First module in each bottlneck is the only one utilizing stride=2 rest using stride=1
		
			 
		self.conv1 = nn.Conv2d (3, 32, kernel_size=3, stride=2, padding=1, bias = False)
		self.bn1 = nn.BatchNorm2d(32)
		self.relu1 = nn.ReLU6()

		self.linear_bottleneck_0_1 = LinearBottleneck(32, 16, stride=1, t=1)

		self.linear_bottleneck_1_0 = LinearBottleneck(16, 24, stride=2, t=6)
		self.linear_bottleneck_1_1 = LinearBottleneck(24, 24, stride=1, t=6)

		self.linear_bottleneck_2_0 = LinearBottleneck(24, 32, stride=2, t=6)
		self.linear_bottleneck_2_1 = LinearBottleneck(32, 32, stride=1, t=6)
		self.linear_bottleneck_2_2 = LinearBottleneck(32, 32, stride=1, t=6)

		self.linear_bottleneck_3_0 = LinearBottleneck(32, 64, stride=2, t=6)
		self.PSABlock_1 = PooledSpatialAttentionBlockV5(64, 1, 14, 14)
		self.PSABlock_2 = PooledSpatialAttentionBlockV5(64, 1, 14, 14)
		self.PSABlock_3 = PooledSpatialAttentionBlockV5(64, 1, 14, 14)

		self.linear_bottleneck_4_0 = LinearBottleneck(64, 96, stride=1, t=6)
		self.linear_bottleneck_4_1 = LinearBottleneck(96, 96, stride=1, t=6)
		self.linear_bottleneck_4_2 = LinearBottleneck(96, 96, stride=1, t=6)

		self.linear_bottleneck_5_0 = LinearBottleneck(96, 160, stride=2, t=6)
		self.linear_bottleneck_5_1 = LinearBottleneck(160, 160, stride=1, t=6)
		self.linear_bottleneck_5_2 = LinearBottleneck(160, 160, stride=1, t=6)

		self.linear_bottleneck_6_0 = LinearBottleneck(160, 320, stride=1, t=6)

		self.conv_last = nn.Conv2d(320, 1280, kernel_size=(1, 1), stride=(1, 1), bias=False)
		self.bn_last = nn.BatchNorm2d(1280, eps=1e-05, momentum=0.1)
		self.avgpool = nn.AdaptiveAvgPool2d(output_size=1)
		self.dropout = nn.Dropout(p=0.2, inplace=True)
		self.fc = nn.Linear(in_features=1280, out_features=num_classes, bias=True)
	

	def forward(self, x):
		out = self.conv1(x)
		out = self.bn1(out)
		out = self.relu1(out)

		out = self.linear_bottleneck_0_1(out)
		
		out = self.linear_bottleneck_1_0(out)
		out = self.linear_bottleneck_1_1(out)

		out = self.linear_bottleneck_2_0(out)
		out = self.linear_bottleneck_2_1(out)
		out = self.linear_bottleneck_2_2(out)

		out = self.linear_bottleneck_3_0(out)
		out = self.PSABlock_1(out)
		out = self.PSABlock_2(out)
		out = self.PSABlock_3(out)

		out = self.linear_bottleneck_4_0(out)
		out = self.linear_bottleneck_4_1(out)
		out = self.linear_bottleneck_4_2(out)

		out = self.linear_bottleneck_5_0(out)
		out = self.linear_bottleneck_5_1(out)
		out = self.linear_bottleneck_5_2(out)

		out = self.linear_bottleneck_6_0(out)

		out = self.conv_last(out)
		out = self.bn_last(out)
		out = self.avgpool(out)
		out = self.dropout(out)
		out = out.view(out.size(0), -1)
		out = self.fc(out)

		return F.log_softmax(out, dim=1)
