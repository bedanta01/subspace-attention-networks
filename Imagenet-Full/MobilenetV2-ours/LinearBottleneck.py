import torch
import torch.nn as nn

class LinearBottleneck(nn.Module):
	def __init__(self, inplanes, outplanes, stride=1, t=6, activation=nn.ReLU6):
		super(LinearBottleneck, self).__init__()
		self.conv1 = nn.Conv2d(inplanes, inplanes * t, kernel_size=1, bias=False)
		self.bn1 = nn.BatchNorm2d(inplanes * t)
		self.conv2 = nn.Conv2d(inplanes * t, inplanes * t, kernel_size=3, stride=stride, padding=1, bias=False,
							   groups=inplanes * t)
		self.bn2 = nn.BatchNorm2d(inplanes * t)
		self.conv3 = nn.Conv2d(inplanes * t, outplanes, kernel_size=1, bias=False)
		self.bn3 = nn.BatchNorm2d(outplanes)
		self.activation = activation(inplace=True)
		self.stride = stride
		self.t = t
		self.inplanes = inplanes
		self.outplanes = outplanes

	def forward(self, x):
		residual = x

		out = self.conv1(x)
		out = self.bn1(out)
		out = self.activation(out)

		out = self.conv2(out)
		out = self.bn2(out)
		out = self.activation(out)

		out = self.conv3(out)
		out = self.bn3(out)

		if self.stride == 1 and self.inplanes == self.outplanes:
			out += residual

		return out
