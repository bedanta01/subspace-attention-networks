from collections import OrderedDict

import torch
import torch.nn as nn
from torch.autograd import Variable

torch.set_default_tensor_type(torch.cuda.FloatTensor)

class SplitArm(nn.Module):
	def __init__(self, nin):
		super(SplitArm, self).__init__()
		self.conv_dws = nn.Conv2d(nin, nin, kernel_size=1, stride=1, padding=0, groups=nin)
		self.bn_dws = nn.BatchNorm2d(nin, momentum=0.9)
		self.relu_dws = nn.ReLU(inplace=True)

		self.maxpool = nn.MaxPool2d(kernel_size=3, stride=1, padding=1)

		self.conv_point = nn.Conv2d(nin, 1, kernel_size=1, stride=1, padding=0, groups=1)
		self.bn_point = nn.BatchNorm2d(1, momentum=0.9)
		self.relu_point = nn.ReLU(inplace=True)

		self.softmax = nn.Softmax(dim=2)

	def forward(self, x):
		out = self. conv_dws(x)
		out = self.bn_dws(out)
		out = self.relu_dws(out)

		out = self.maxpool(x)

		out = self.conv_point(out)
		out = self.bn_point(out)
		out = self.relu_point(out)

		m, n, p, q = out.shape
		out = self.softmax(out.view(m, n, -1))
		out = out.view(m, n, p, q)

		out = out.expand(x.shape[0], x.shape[1], x.shape[2], x.shape[3])

		out = torch.mul(out, x)

		out = out + x
		
		return out

class AlphaBlock(nn.Module):
	def __init__(self, nin, nout, h, w, num_splits):
		super(AlphaBlock, self).__init__()
		self.split = self._make_splits(nin, nout, h, w, num_splits)
		
		self.nin = nin
		self.nout = nout
		self.h = h
		self.w = w
		self.num_splits = num_splits

		self.arm_1 = SplitArm(int(self.nin/self.num_splits))
		self.arm_2 = SplitArm(int(self.nin/self.num_splits))
		self.arm_3 = SplitArm(int(self.nin/self.num_splits))
		self.arm_4 = SplitArm(int(self.nin/self.num_splits))

		# self.arm_5 = SplitArm(int(self.nin/self.num_splits))
		# self.arm_6 = SplitArm(int(self.nin/self.num_splits))
		# self.arm_7 = SplitArm(int(self.nin/self.num_splits))
		# self.arm_8 = SplitArm(int(self.nin/self.num_splits))

		# self.arm_9 = SplitArm(int(self.nin/self.num_splits))
		# self.arm_10 = SplitArm(int(self.nin/self.num_splits))
		# self.arm_11 = SplitArm(int(self.nin/self.num_splits))
		# self.arm_12 = SplitArm(int(self.nin/self.num_splits))

		# self.arm_13 = SplitArm(int(self.nin/self.num_splits))
		# self.arm_14 = SplitArm(int(self.nin/self.num_splits))
		# self.arm_15 = SplitArm(int(self.nin/self.num_splits))
		# self.arm_16 = SplitArm(int(self.nin/self.num_splits))

	def forward(self, x):
		group_size = int(self.nin/self.num_splits)
		x_split = []
		batch_size = x.shape[0]

		start = 0
		end = group_size

		for i in range(self.num_splits):
			x_split.append(x[ : , start:end, :, :])
			start = end
			end = end + group_size

		# out = Variable(torch.zeros(batch_size, int(self.nout/self.num_splits), int(self.h), int(self.w)))

		# for key, module in (self.split).items():
		# 	maps = ((self.split)[key])(x_split[key])

		# 	if key == 0:
		# 		out = out + maps
		# 	else:
		# 		out = torch.cat((out, maps), dim=1)

		# # if torch.all(torch.eq(x, out)):
		# # 	print("Same")

		out1 = self.arm_1(x_split[0])
		out2 = self.arm_2(x_split[1])
		out3 = self.arm_3(x_split[2])
		out4 = self.arm_4(x_split[3])

		# out5 = self.arm_5(x_split[4])
		# out6 = self.arm_6(x_split[5])
		# out7 = self.arm_7(x_split[6])
		# out8 = self.arm_8(x_split[7])

		# out9 = self.arm_9(x_split[8])
		# out10 = self.arm_10(x_split[9])
		# out11 = self.arm_11(x_split[10])
		# out12 = self.arm_12(x_split[11])

		# out13 = self.arm_13(x_split[12])
		# out14 = self.arm_14(x_split[13])
		# out15 = self.arm_15(x_split[14])
		# out16 = self.arm_16(x_split[15])

		# out = torch.cat((out1, out2, out3, out4, out5, out6, out7, out8, out9, out10, out11, out12, out13, out14, out15, out16), dim=1)
		out = torch.cat((out1, out2, out3, out4), dim=1)

		return out

	def _make_splits(self, nin, nout, h, w, num_splits):

		modules = OrderedDict()
		arm_name = "arm"
		split_channels = int(nin/num_splits)

		for i in range(0, num_splits):
			# name = arm_name + "_{}".format(i)
			name = i
			_module = SplitArm(split_channels)
			modules[name] = _module

		return modules

# print(AlphaBlock(64, 64, 112, 112, 4))
			

