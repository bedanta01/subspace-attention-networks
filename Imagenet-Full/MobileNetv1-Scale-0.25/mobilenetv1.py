import torch
import torch.nn as nn

scale=0.25

class mobilenetv1(nn.Module):
	def __init__(self, num_classes):
		super(mobilenetv1, self).__init__()

		def conv_bn(inp, oup, stride):
			return nn.Sequential(
				nn.Conv2d(inp, oup, 3, stride, 1, bias=False),
				nn.BatchNorm2d(oup),
				nn.ReLU(inplace=True)
			)

		def conv_dw(inp, oup, stride):
			return nn.Sequential(
				nn.Conv2d(inp, inp, 3, stride, 1, groups=inp, bias=False),
				nn.BatchNorm2d(inp),
				nn.ReLU(inplace=True),
	
				nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
				nn.BatchNorm2d(oup),
				nn.ReLU(inplace=True),
			)

		self.model = nn.Sequential(
			conv_bn(  3,  int(32*scale), 2), 
			conv_dw( int(32*scale),  int(64*scale), 1),
			conv_dw( int(64*scale), int(128*scale), 2),
			conv_dw(int(128*scale), int(128*scale), 1),
			conv_dw(int(128*scale), int(256*scale), 2),
			conv_dw(int(256*scale), int(256*scale), 1),
			conv_dw(int(256*scale), int(512*scale), 2),
			conv_dw(int(512*scale), int(512*scale), 1),
			conv_dw(int(512*scale), int(512*scale), 1),
			conv_dw(int(512*scale), int(512*scale), 1),
			conv_dw(int(512*scale), int(512*scale), 1),
			conv_dw(int(512*scale), int(512*scale), 1),
			conv_dw(int(512*scale), int(1024*scale), 2),
			conv_dw(int(1024*scale), 1024, 1),
			nn.AvgPool2d(7),
		)
		self.fc = nn.Linear(1024, num_classes)

	def forward(self, x):
		x = self.model(x)
		x = x.view(-1, 1024)
		x = self.fc(x)
		return x
		