"""

	H^(2) idea, but not H^2

"""

import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from ConvLayer import ConvLayer

class SVABloc(nn.Module):
    def __init__(self, nin, nout, kernel_size, h, w):
        super(SVABloc, self).__init__()
        self.conv_depthwise = nn.Conv2d(nin, nin, kernel_size=kernel_size, padding=0, groups=1)
        torch.nn.init.xavier_uniform_(self.conv_depthwise.weight)
        self.conv_batchnorm = nn.BatchNorm2d(nin, momentum=0.9)
        self.conv_bloc1 = nn.Conv2d(h, nout, kernel_size=kernel_size)
        self.conv_batchnorm_1 = nn.BatchNorm2d(nout, momentum=0.9)
        self.softmax2d = nn.Softmax(dim=2) # make sure about dim in 2d
    
    def forward(self, x):
        
        _b, _m, _h, _w = x.shape
        
        ###########	depthwise convolution	##############

        out_depthwise = self.conv_depthwise(x)
        #print("after depthwise: ", out_depthwise.shape)

        out_depthwise = self.conv_batchnorm(out_depthwise)

        #######################################################


        ################	transpose	#######################
        out_depthwise1 = torch.transpose(out_depthwise, 1, 2)
        # print(" after transpose: ",out1.shape)


        
        ###############		generate attention maps #############
        #########################################################

        out_conv_bloc1 = self.conv_bloc1(out_depthwise1)
        # print("attention_maps: ",out_conv_bloc1.shape)

        out_conv_bloc1 = self.conv_batchnorm_1(out_conv_bloc1)
        
        b, h, m, w = out_conv_bloc1.shape

        out_conv_bloc1 = out_conv_bloc1.view(b, h, -1)
        attention_maps = self.softmax2d(out_conv_bloc1)
        # print("attention_maps: ",attention_maps.shape)

        ##################	Bilinear pooling 1 ######################
        ##########################################################3
        q, w, e, r = out_depthwise1.shape

        # print(out_depthwise1.shape)
        out_depthwise1 = out_depthwise1.contiguous().view(q, w, -1)#, out_depthwise1.shape[2]*out_depthwise1.shape[3])
        #print("out1: ",out_depthwise1.shape, attention_maps.shape)

        out_H = torch.bmm(out_depthwise1, torch.transpose(attention_maps, 1, 2))
        # print("after bilinear pooling: ",out_H.shape, out_depthwise1.shape)
        out_H = F.normalize(out_H, dim=0)


        out_H2 = torch.bmm(torch.transpose(out_depthwise1, 1, 2), out_H)
        # print("after Mat mul: ",out_H.shape, out_H2.shape)


        ######################  append to depthwise output ###############
        ##################################################################

        z = torch.bmm(out_H, torch.transpose(out_H2, 1, 2))
        # print("z dimensions: ", z.shape)

        z = z.view(_b, _h, _m, _w)

        z = torch.transpose(z, 1, 2)
        # print("z dimensions: ", z.shape)

        # print("z dimensions: ", z.shape)

        return z	