import os
import PIL
import numpy as np
import torch
import torch.nn.functional as F
import torch.nn as nn
import argparse

import torchvision.models as models
from torchvision.utils import make_grid, save_image

import time

from utils import visualize_cam, Normalize
from gradcam import GradCAM, GradCAMpp

# from mobilenetv1_vanilla import mobilenetv1
from mobilenetv1_8groups import mobilenetv1
# from modelours_V6_ import MobileNet2
# from modelours import MobileNet2


parser = argparse.ArgumentParser(description='PyTorch ImageNet Training')
parser.add_argument('--model', default='', type=str, metavar='MODEL_NAME',help='path to latest checkpoint (default: none)')
parser.add_argument('--path', default='', type=str, metavar='PATH',help='path to latest checkpoint (default: none)')

args = parser.parse_args()

MODEL_NAME = args.model
img_folder = args.path


os.environ["CUDA_VISIBLE_DEVICES"]="0"

MODEL_PATH = "/raid/cs17mtech11009/r/gradcam-vis/" + MODEL_NAME


img_dir = "/raid/cs17mtech11009/r/gradcam-vis/images/"+img_folder
output_dir = '/raid/cs17mtech11009/r/gradcam-vis/outputs/'+img_folder

if(not os.path.exists(output_dir)):
	os.mkdir(output_dir)

input_files = os.listdir(img_dir)
# img_name = "Forsters_Tern_0083_151282.jpg"

num_classes = 200
model = mobilenetv1(num_classes)
# model = MobileNet2(input_size=224, scale=1, num_classes=num_classes)

model.cuda()
checkpoint = torch.load(MODEL_PATH)
model.load_state_dict(checkpoint['state_dict'],strict=False)
model.eval()

print("Layers:")
for name, param in model.named_parameters():
    if param.requires_grad:
        print(name)

for idx, img_name in enumerate(input_files): 
	# print("Generating: ", idx)
	img_path = os.path.join(img_dir, img_name)

	pil_img = PIL.Image.open(img_path)

	normalizer = Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
	torch_img = torch.from_numpy(np.asarray(pil_img)).permute(2, 0, 1).unsqueeze(0).float().div(255).cuda()
	torch_img = F.interpolate(torch_img, size=(224, 224), mode='bilinear', align_corners=False)
	normed_torch_img = normalizer(torch_img)

	cam_dict = dict()




	_layer = ""
	mobilenet_model_dict = dict(type='mobilenet', arch=model, layer_name=_layer, input_size=(224, 224))
	mobilenet_gradcam = GradCAM(mobilenet_model_dict, True)
	mobilenet_gradcampp = GradCAMpp(mobilenet_model_dict, True)

	cam_dict['mobilenet'] = [mobilenet_gradcam, mobilenet_gradcampp]

	images = []
	for gradcam, gradcam_pp in cam_dict.values():
	    mask, _ = gradcam(normed_torch_img)
	    heatmap, result = visualize_cam(mask, torch_img)

	    mask_pp, _ = gradcam_pp(normed_torch_img)
	    heatmap_pp, result_pp = visualize_cam(mask_pp, torch_img)
	    
	    images.append(torch.stack([torch_img.squeeze().cpu(), heatmap, heatmap_pp, result, result_pp], 0))
	    
	images = make_grid(torch.cat(images, 0), nrow=5)

	os.makedirs(output_dir, exist_ok=True)
	timestamp = str(time.time())
	output_name = img_name.replace('.jpg', '') + "-"+ MODEL_NAME.replace(".pth.tar","")+ "-" +timestamp.replace('.', '') +".jpg"
	output_path = os.path.join(output_dir, output_name)

	save_image(images, output_path)

print("-----------------------------")
