import argparse
import os
import shutil
import time

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models
from TinyImageNet import TinyImageNet

from ConvLayer import ConvLayer
from tensorboardX import SummaryWriter
from utils import show_images_horizontally

from NonLocalBlock import NonLocal

batch_size = 224
learning_rate = 0.01
n_epochs = 160
iteration = 0
# root = '/home/cs17mtech11002/project101/TinyImagenet'

_folder = 'logs-1-0'
writer5 = SummaryWriter(_folder+'/exp-5')

cudnn.benchmark = True
os.environ["CUDA_VISIBLE_DEVICES"]="0, 1, 3, 4, 5, 6, 7"

root = '/raid/cs17mtech11009/datasets/TinyImagenet'

normalize = transforms.Normalize((.5, .5, .5), (.5, .5, .5))

augmentation = transforms.RandomApply([
	transforms.RandomHorizontalFlip(),
	transforms.RandomRotation(10),
	transforms.RandomResizedCrop(64)], p=.8)

training_transform = transforms.Compose([
	transforms.Lambda(lambda x: x.convert("RGB")),
	augmentation,
	transforms.ToTensor(),
	normalize])

valid_transform = transforms.Compose([
	transforms.Lambda(lambda x: x.convert("RGB")),
	transforms.ToTensor(),
	normalize])

class Net(nn.Module):
	def __init__(self):
		super(Net, self).__init__()

		# sees 64x64
		self.conv1 = ConvLayer(3, 16, 3, stride=2, padding=1)
		
		# sees 32x32
		self.conv2 = ConvLayer(16, 32, 3, padding=1)




		# sees 	32x32	
		self.conv3 = ConvLayer(32, 32, 3, padding=1)

		# sees 32X32
		self.conv4 = ConvLayer(32, 64, 3, stride=2, padding=1)



		# sees 16x16
		self.conv5 = ConvLayer(64, 64, 3, padding=1)

		# sees 16x16
		self.conv6 = ConvLayer(64, 64, 3, padding=1)




		# sees 16x16
		self.conv7 = ConvLayer(64, 128, 3, stride=2, padding=1)


		# sees 8x8
		self.conv8 = ConvLayer(128, 128, 3, padding=1)


		self.NonLocalBlock_1 = NonLocal(128, 128, 1, 64)


		# sees 8x8
		self.conv9 = ConvLayer(128, 256, 3, padding=1)


		# sees 8x8
		self.conv10 = ConvLayer(256, 256, 3, padding=1)


		self.fc1 = nn.Linear(256*8*8, 2048)
		self.fc1_bn = nn.BatchNorm1d(2048)		
		self.fc1_relu = nn.ReLU()
		self.fc1_dropout = nn.Dropout(0.5)

		self.fc2 = nn.Linear(2048, 200)


	def forward(self, x):
		
		out = self.conv1(x)
		#print(out.shape)
		out = self.conv2(out)


		#print(out.shape)
		out = self.conv3(out)


		#print(out.shape)
		out = self.conv4(out)
		

		#print(out.shape)
		out = self.conv5(out)
		#print(out.shape)
		out = self.conv6(out)


		#print(out.shape)
		out = self.conv7(out)


		#print(out.shape)
		out = self.conv8(out)
		
		out = self.NonLocalBlock_1(out)

		#print(out.shape)
		out = self.conv9(out)


		#print(out.shape)
		out = self.conv10(out)
		#print(out.shape)


		out = out.view(-1, 256*8*8)
		
		out = self.fc1(out)
		out = self.fc1_bn(out)
		out = self.fc1_relu(out)
		
		out = self.fc1_dropout(out)

		out = self.fc2(out)

		return out

def main():
	global args
	best_prec1 = 0

	cudnn.benchmark = True

	model = Net()
	# print(model)
	model = nn.DataParallel(model).cuda()
	
	criterion = nn.CrossEntropyLoss().cuda()

	optimizer = torch.optim.SGD(model.parameters(), learning_rate,
								momentum=0.9,
								weight_decay=1e-4)

	normalize = transforms.Normalize(mean=[0.5, 0.5, 0.5],
									 std=[0.5, 0.5, 0.5])

	train_loader = torch.utils.data.DataLoader(TinyImageNet(root, 'train', transform=training_transform, in_memory=False),
		batch_size=batch_size, shuffle=True,
		num_workers=90, pin_memory=True)

	val_loader = torch.utils.data.DataLoader(TinyImageNet(root, 'val', transform=valid_transform, in_memory=False),
		batch_size=batch_size, shuffle=False,
		num_workers=90, pin_memory=True)

	# tmpiter = iter(train_loader)
	# for _ in range(5):
	# 	images, labels = tmpiter.next()
	# 	show_images_horizontally(images, un_normalize=True)

	# print(len(val_loader))

	writer1 = SummaryWriter(_folder+'/exp-1')
	writer2 = SummaryWriter(_folder+'/exp-2')
	writer3 = SummaryWriter(_folder+'/exp-3')
	writer4 = SummaryWriter(_folder+'/exp-4')

	starttime = time.time()

	for epoch in range(1, n_epochs+1):
		adjust_learning_rate(optimizer, epoch)

		# train for one epoch
		train_acc, train_top1_avg, train_top5_avg, train_losses_avg = train(train_loader, model, criterion, optimizer, epoch)

		# evaluate on validation set
		test_acc, test_top1_avg, test_top5_avg, test_losses_avg = validate(val_loader, model, criterion)



		writer1.add_scalar("Training loss", train_losses_avg, epoch)
		writer2.add_scalar("Testing loss", test_losses_avg, epoch)
		writer3.add_scalar("Top1 accuracy", test_top1_avg, epoch)
		writer4.add_scalar("Top5 accuracy", test_top5_avg, epoch)

		starttime = time.time() - starttime

		print('Ep: {}   EpTime: {:.4f} 	 Trainloss: {:.4f}   Testloss: {:.4f}  trainAcc {:.2f} top1 {:.4f}   top5 {:.4f}'.format(epoch, starttime,train_losses_avg, test_losses_avg, train_acc ,  test_top1_avg, test_top5_avg))

		starttime = time.time()

		# remember best prec@1 and save checkpoint
		# is_best = test_top1_avg > best_prec1
		# best_prec1 = max(test_top1_avg, best_prec1)
		# save_checkpoint({
		# 	'epoch': epoch + 1,
		# 	'state_dict': model.state_dict(),
		# 	'best_prec1': best_prec1,
		# 	'optimizer' : optimizer.state_dict(),
		# }, is_best)


def train(train_loader, model, criterion, optimizer, epoch):
	losses = AverageMeter()
	top1 = AverageMeter()
	top5 = AverageMeter()
	batch_time = AverageMeter()
	data_time = AverageMeter()
	
	# switch to train mode
	model.train()
	correct = 0
	total = 0
	end = time.time()
	
	for i, (input, target) in enumerate(train_loader):
		# measure data loading time
		data_time.update(time.time() - end)

		target = target.cuda(async=True)
		input = input.cuda()

		# compute output
		output = model(input)
		loss = criterion(output, target)
		_, pred = torch.max(output, 1)

		torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)

		prec1, prec5 = accuracy(output.data, target, topk=(1, 5))
		losses.update(loss.item(), input.size(0))
		top1.update(prec1.item(), input.size(0))
		top5.update(prec5.item(), input.size(0))

		# compute gradient and do SGD step
		optimizer.zero_grad()
		loss.backward()
		optimizer.step()

		correct += (pred == target).sum().item()
		total += target.size(0)
		
		batch_time.update(time.time() - end)
		end = time.time()

		# print('Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
		# 	  'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'.format(
		# 	  	batch_time=batch_time,data_time=data_time))
		# print(' * Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f}'.format(top1=top1, top5=top5))

	train_acc = 100.*(correct/total)
	return train_acc, top1.avg, top5.avg, losses.avg


def validate(val_loader, model, criterion):
	losses = AverageMeter()
	top1 = AverageMeter()
	top5 = AverageMeter()

	model.eval()
	correct = 0
	total = 0
	global iteration

	for i, (input, target) in enumerate(val_loader):
		iteration = iteration + 1

		target = target.cuda(async=True)
		input = input.cuda()

		# compute output
		output = model(input)
		loss = criterion(output, target)
		_, pred = torch.max(output, 1)

		prec1, prec5 = accuracy(output.data, target, topk=(1, 5))
		losses.update(loss.item(), input.size(0))
		top1.update(prec1.item(), input.size(0))
		top5.update(prec5.item(), input.size(0))

		writer5.add_scalar("Batchwise Testloss", loss.item(), iteration)

		correct += (pred == target).sum().item()
		total += target.size(0)
		
		# print(' * Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f}'.format(top1=top1, top5=top5))

	val_acc = 100.*(correct/total)
	# print(correct, total)
	return val_acc, top1.avg, top5.avg, losses.avg

def adjust_learning_rate(optimizer, epoch):
	"""Sets the learning rate to the initial LR decayed by 1/10 every 30 epochs"""
	lr = learning_rate * (0.1 ** (epoch // 30))
	for param_group in optimizer.param_groups:
		param_group['lr'] = lr

def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
	torch.save(state, filename)
	if is_best:
		shutil.copyfile(filename, 'model_best.pth.tar')

class AverageMeter(object):
	"""Computes and stores the average and current value"""
	def __init__(self):
		self.reset()

	def reset(self):
		self.val = 0
		self.avg = 0
		self.sum = 0
		self.count = 0

	def update(self, val, n=1):
		self.val = val
		self.sum += val * n
		self.count += n
		self.avg = self.sum / self.count

def accuracy(output, target, topk=(1,)):
	"""Computes the precision@k for the specified values of k"""
	maxk = max(topk)
	batch_size = target.size(0)

	_, pred = output.topk(maxk, 1, True, True)
	pred = pred.t()
	correct = pred.eq(target.view(1, -1).expand_as(pred))

	res = []
	for k in topk:
		correct_k = correct[:k].view(-1).float().sum(0)
		res.append(correct_k.mul_(100.0 / batch_size))
	return res
if __name__ == '__main__':
	main()