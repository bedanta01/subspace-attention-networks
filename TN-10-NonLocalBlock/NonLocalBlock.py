"""

	A Non Local Module

"""

import torch
import torch.nn as nn

class NonLocal(nn.Module):

	# bottleneck channel half of the input channels mentioned in paper
	
	def __init__(self, in_channel, out_channel, kernel_size, bottleneck_channel, stride=1, padding=0):
		super(NonLocal, self).__init__()
		self.conv_theta = nn.Conv2d(in_channels=in_channel, out_channels=bottleneck_channel, kernel_size=kernel_size,stride=stride, padding = padding)
		self.conv_phi = nn.Conv2d(in_channels=in_channel, out_channels=bottleneck_channel, kernel_size=kernel_size,stride=stride, padding = padding)
		self.conv_g = nn.Conv2d(in_channels=in_channel, out_channels=bottleneck_channel, kernel_size=kernel_size,stride=stride, padding = padding)

		self.conv0 = nn.Conv2d(in_channels=bottleneck_channel, out_channels=out_channel, kernel_size=kernel_size,stride=stride, padding = padding)

		self.softmax2d = nn.Softmax(dim=2) # make sure about dim in 2d

	def forward(self, x):

		b,c,h,w = x.shape
		# print("x ", x.shape)
		out_conv_theta = self.conv_theta(x)
		out_conv_theta = out_conv_theta.reshape(out_conv_theta.shape[0], out_conv_theta.shape[1], -1)

		# print("theta ", out_conv_theta.shape)

		out_conv_phi = self.conv_phi(x)
		out_conv_phi = out_conv_phi.reshape(out_conv_phi.shape[0], out_conv_phi.shape[1], -1)

		# print("Phi ", out_conv_phi.shape)


		out_conv_g = self.conv_g(x)
		out_conv_g = out_conv_g.reshape(out_conv_g.shape[0], out_conv_g.shape[1], -1)

		# print("G ",out_conv_g.shape)

		# print("theta ", out_conv_theta.shape)
		# print("Phi ", out_conv_phi.shape)

		# matrix multiplication
		out_mat_mul = torch.bmm(torch.transpose(out_conv_theta, 1, 2), out_conv_phi) # output = (b.c.c) or equivalent to (b.m.n) according to paper

		# print(out_mat_mul.shape)

		attention = self.softmax2d(out_mat_mul)

		# print("attention", attention.shape)

		_y = torch.bmm(attention, torch.transpose(out_conv_g,1,2)) # output = (b.c.c) or equivalent to (b.m.n) according to paper
		# print("Y ", _y.shape)

		_y = torch.transpose(_y, 1, 2)
		_y = _y.reshape(_y.shape[0], _y.shape[1], h, w)
		# print("Y ", _y.shape)

		out_conv0 = self.conv0(_y)
		# print(x.shape, out_conv0.shape)

		z = x + out_conv0.expand_as(x)
		# print(z.shape)

		return z
