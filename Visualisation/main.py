import os
import shutil
import time
import warnings

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models
import matplotlib.pyplot as plt
from torchvision.utils import make_grid
from copy import deepcopy

from mobilenetv1 import mobilenetv1

root_folder = '/raid/cs17mtech11009/datasets/BirdDataset'
valdir = os.path.join(root_folder, 'test')
batch_size = 1
num_classes = 200

os.environ["CUDA_VISIBLE_DEVICES"]="0"

MODEL_PATH = '/raid/cs17mtech11009/r/Visualisation/MobileNet-v1-SD63_model_best.pth.tar'

normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
										 std=[0.229, 0.224, 0.225])

val_loader = torch.utils.data.DataLoader(
				datasets.ImageFolder(valdir, transforms.Compose([
				transforms.Resize(256),
				transforms.CenterCrop(224),
				transforms.ToTensor(),
				normalize,
			])),
			batch_size=batch_size, shuffle=False,
			num_workers=16, pin_memory=True)

model = mobilenetv1(num_classes)
model = nn.DataParallel(model).cuda()


criterion = nn.CrossEntropyLoss().cuda()
optimizer = torch.optim.SGD(model.parameters(), 0.1,
								momentum=0.9,
								weight_decay=4e-5)



print("Loading model:")
checkpoint = torch.load(MODEL_PATH)
model.load_state_dict(checkpoint['state_dict'],strict=False)
# optimizer.load_state_dict(checkpoint['optimizer'])
print("Model loaded:")
model.eval()

print("Layers:")
for name, param in model.named_parameters():
    if param.requires_grad:
        print(name)

def save_conv_filters(model):
	kernels = model.module.bottlenecks.Bottlenecks_5.LinearBottleneck5_0.conv2.weight.detach().clone()
	kernels = kernels - kernels.min()
	kernels = kernels / kernels.max()
	print(kernels.shape)
	img = make_grid(kernels)
	img.permute(1, 2, 0)
	torchvision.utils.save_image(img, "kernel1.png")
	# fig, axarr = plt.subplots(kernels.size(0))
	# print(kernels.shape)
	# for idx in range(kernels.size(0)):
	#     axarr[idx].imshow(kernels[idx].squeeze())

# save_conv_filters(model)

def validate(val_loader, model, criterion):
	losses = AverageMeter()
	top1 = AverageMeter()
	top5 = AverageMeter()
	batch_time = AverageMeter()

	model.eval()
	correct = 0
	total = 0

	activations = {}
	
	def get_activation(name):
		def hook(model, input, output):
			activations[name] = output.detach()
			test = deepcopy(activations)
		return hook

	for i, (input, target) in enumerate(val_loader):

		target = target.cuda(async=True)
		input = input.cuda()
		
		model.module.model[11][0].register_forward_hook(get_activation('module.model[11][0]'))

		output = model(input)
		loss = criterion(output, target)
		_, pred = torch.max(output, 1)

		imgs = activations['module.model[11][0]']
		print(imgs)
		imgs = imgs.squeeze()
		imgs = imgs.unsqueeze(dim=1)
		imgs = make_grid(imgs)
		# imgs = imgs.permute(1, 0, 2, 3)
		# print(imgs.shape)
		trans = torchvision.transforms.ToPILImage()
		imgs = trans(imgs)
		# o = nn.functional.interpolate(imgs, size=(224,224), mode='bilinear')
		torchvision.utils.save_image(imgs, "kernel.png")
		
		prec1, prec5 = accuracy(output.data, target, topk=(1, 5))
		losses.update(loss.item(), input.size(0))
		top1.update(prec1.item(), input.size(0))
		top5.update(prec5.item(), input.size(0))

		correct += (pred == target).sum().item()
		total += target.size(0)
		
		# if i % 10 == 0:
		# 	print('Test: [{0}/{1}]\t'
		# 		  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
		# 		  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
		# 		  'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
		# 		  'Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
		# 		   i, len(val_loader), batch_time=batch_time, loss=losses,
		# 		   top1=top1, top5=top5))
		break
	print(' * Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f}'
		  .format(top1=top1, top5=top5))
	
	val_acc = 100.*(correct/total)
	# print(correct, total)
	return val_acc, top1.avg, top5.avg, losses.avg

class AverageMeter(object):
	def __init__(self):
		self.reset()

	def reset(self):
		self.val = 0
		self.avg = 0
		self.sum = 0
		self.count = 0

	def update(self, val, n=1):
		self.val = val
		self.sum += val * n
		self.count += n
		self.avg = self.sum / self.count

def accuracy(output, target, topk=(1,)):
	"""Computes the precision@k for the specified values of k"""
	maxk = max(topk)
	batch_size = target.size(0)

	_, pred = output.topk(maxk, 1, True, True)
	pred = pred.t()
	correct = pred.eq(target.view(1, -1).expand_as(pred))

	res = []
	for k in topk:
		correct_k = correct[:k].view(-1).float().sum(0)
		res.append(correct_k.mul_(100.0 / batch_size))
	return res

test_acc, test_top1_avg, test_top5_avg, test_losses_avg = validate(val_loader, model, criterion)
