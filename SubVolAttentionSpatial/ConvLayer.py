"""

	A 2D Conv layer module

"""

import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

class ConvLayer(nn.Module):

	def __init__(self, in_channel, out_channel, kernel_size, stride=1, padding=0):
		super(ConvLayer, self).__init__()
		self.conv0 = nn.Conv2d(in_channels=in_channel,out_channels=out_channel,kernel_size=kernel_size,stride=stride, padding = padding)
		torch.nn.init.xavier_uniform_(self.conv0.weight)
		# self.conv0_bn = nn.BatchNorm2d(out_channel, momentum=0.9)
		self.relu = nn.RReLU()

	def forward(self, x):
		out_conv0 = self.conv0(x)
		out_relu0 = self.relu(out_conv0)
		return out_relu0
