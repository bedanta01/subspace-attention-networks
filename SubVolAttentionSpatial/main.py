"""
	implementation of A2 Net

"""
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torchvision import datasets
import torchvision.transforms as transforms
from torch.utils.data.sampler import SubsetRandomSampler
import torch.optim as optim
import time, datetime
from tensorboardX import SummaryWriter

from ConvLayer import ConvLayer
from SubVolumeAttentionSpatial import SVABloc

batch_size = 32
learning_rate = 0.001
n_epochs = 100
iteration = 0

print("Checking system configuration...")

gpu_available = torch.cuda.is_available()

if not gpu_available:
	print("GPU not available")
else:
	print("GPU available")

backends_enabled =  torch.backends.cudnn.enabled

if not backends_enabled:
	print("backends not enabled")
else:
	print("Backends enabled")

print("All set...")

transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010))
    ])

train_data = datasets.CIFAR10('data', train=True, download = True, transform = transform)
test_data = datasets.CIFAR10('data', train=False, download = True, transform = transform)


train_loader = torch.utils.data.DataLoader(train_data, batch_size = batch_size, shuffle = True, num_workers = 0)
test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle = False, num_workers=0)

classes = ['airplane', 'automobile', 'bird', 'cat', 'deer',
           'dog', 'frog', 'horse', 'ship', 'truck']

class network(nn.Module):
	def __init__(self):
		super(network, self).__init__()
		
		# sees 32X32
		self.conv1 = ConvLayer(3, 16, 5)
		
		# sees 28X28
		self.conv2 = ConvLayer(16, 32, 1)

		# sees 	28X28	
		self.conv3 = ConvLayer(32, 64, 3, stride=2)
		#self.dropout_1 = nn.Dropout(0.2)

		#self.conv3_dws = depthwise_conv(64, 64, 3)

		# sees 13X13
		self.conv4 = ConvLayer(64, 64, 3, stride=1)

		self.svm_bloc1 = SVABloc(64, 128, 1, 11, 11)

		# sees 11 X 11
		self.conv5 = ConvLayer(64, 64, 1,stride=2)
		# self.dropout_3 = nn.Dropout(0.2)

		self.fc1 = nn.Linear(64*6*6, 512)
		self.dropout_3 = nn.Dropout(0.75)

		self.fc2 = nn.Linear(512, 10)
		
		# self.softmax = nn.Softmax(dim=1)



	def forward(self, x1):
		#print(x.shape)
		
		x = self.conv1(x1)
		# print(x.shape)

		x = self.conv2(x)
		# print(x.shape)
		
		x = self.conv3(x)

		#x = self.svm_bloc1(x)

		#x = self.conv3_dws(x)

		
		x = self.conv4(x)

		x = self.svm_bloc1(x)


		x = self.conv5(x)


		x = x.view(-1, 64 * 6 * 6)
		x = self.fc1(x)
		x = self.fc2(x)

		return x

model = network()
print(model)

if gpu_available:
	model.cuda()

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=0.9)
# optimizer = optim.Adagrad(model.parameters(), lr=learning_rate)
# optimizer = optim.Adam(model.parameters(), lr=learning_rate)
# optimizer = optim.RMSprop(model.parameters(), lr=learning_rate, momentum=0.5)

writer1 = SummaryWriter('logs-1-0/exp-1')
writer2 = SummaryWriter('logs-1-0/exp-2')
writer3 = SummaryWriter('logs-1-0/exp-3')
writer4 = SummaryWriter('logs-1-0/exp-4')
writer5 = SummaryWriter('logs-1-0/exp-5')

acc = 0

for epoch in range(1, n_epochs+1):
	acc = 0
	train_loss = 0.0
	valid_loss = 0.0
	test_loss = 0.0
	model.train()

	for data, target in train_loader:
		if gpu_available:
			data, target = data.cuda(), target.cuda()
		optimizer.zero_grad()
		output = model(data)
		loss = criterion(output, target)
		loss.backward()
		optimizer.step()
		torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
		train_loss += loss.item()
		iteration = iteration + 1


	correct = 0;
	total = 0

	model.eval()
	for data, target in test_loader:
	    if gpu_available:
	    	data, target = data.cuda(), target.cuda()
	    output = model(data)
	    loss = criterion(output, target)
	    test_loss += loss.item()*data.size(0)
	    _, pred = torch.max(output, 1)
	    total += target.size(0)
	    correct += (pred == target).sum().item()
	
	acc = 100.00*(correct/total);

	print('Epoch: {}  Train loss: {:.2f}  Test loss: {:.2f}   Acc {:.2f}'.format(epoch, train_loss/len(train_loader), test_loss/len(test_loader), acc))

	writer1.add_scalar("Training loss", train_loss/len(train_loader), epoch)
	writer2.add_scalar("Test loss", test_loss/len(test_loader), epoch)
	writer3.add_scalar("Accuracy", acc, epoch)

	for p in model.parameters():
		if p.grad is not None:
			writer4.add_histogram("grads", p.grad.data, iteration) 
	
	for tag, value in model.named_parameters():
		tag = tag.replace('.', '/')
		if(value.grad is not None):
			writer5.add_histogram(tag, value.data.cpu().numpy(), iteration)
			writer5.add_histogram(tag, value.grad.data.cpu().numpy(), iteration)
	# train_loss = train_loss/len(train_loader.dataset)	
	# valid_loss = valid_loss/len(valid_loader.dataset)

	# print('Epoch: {}  Training Loss: {:.6f}  Validation Loss: {:.6f} Accuracy: {:.2f}'.format(epoch, train_loss, valid_loss, acc))

timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d-%H:%M:%S')
torch.save(model.state_dict(), 'models/double-attention-net-1-0-'+timestamp+'.pt')


