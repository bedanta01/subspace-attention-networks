import matplotlib.pyplot as plt
import numpy as np

# plt.gca().set_color_cycle(['red', 'green'])

data=np.genfromtxt('loss_mv1.csv', comments="#", skip_header=1, delimiter=',', names=['a', 'b', 'c', 'd', 'e'])
plt.plot(data['a'], data['b'],label='the data')
plt.plot(data['a'], data['c'])
plt.plot(data['a'], data['d'])
plt.plot(data['a'], data['e'])

axes = plt.gca()
axes.set_xlim([0,95])
axes.set_ylim([25,80])

plt.xlabel('Epochs')
plt.ylabel('Top-1 Error(%)')
axes.legend(('MobileNet-V1 Train', 'MobileNet-V1 Val','MobileNet-V1 + GrAB Train', 'MobileNet-V1 + GrAB Val'))
axes.set_aspect('auto')
plt.savefig('mobilenet_v1.pdf', bbox_inches="tight")