import pandas as pd
import matplotlib.pyplot as plt

headers = ['epochs', 'MV1-Train (%)', 'MV1 -Val (%)', 'MV1GRAB Train(%)', 'MV1GRAB Val(%)']
df = pd.read_csv('loss_mv1.csv', names=headers)
x = df['epochs']
y =df['MV1-Train (%)']
z =df['MV1 -Val (%)']

plt.plot(x, y)
plt.plot(x, z)

plt.savefig('myfile.pdf')