pat = " * Prec@1"
filename = "mobilnetv1_2__vanilla.out"

array = []

with open(filename, "r") as ins:
	for line in ins:
		# print(line)
		if pat in line:
			# print(line)
			array.append(line.strip('\n'))

	# print(array)

with open(filename+'_extracted', 'w') as f:
	for item in array:
		temp = item.split()
		err = 100-float(temp[-3])
		f.write("%s\n" % str(err))