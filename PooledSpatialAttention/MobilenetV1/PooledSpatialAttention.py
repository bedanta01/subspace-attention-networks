import torch
import torch.nn as nn

class PooledSpatialAttentionBlock(nn.Module):
	def __init__(self, nin, kernel_size, h, w):
		super(PooledSpatialAttentionBlock, self).__init__()
		# self.reflectionPad = nn.ReplicationPad2d(1)
		self.maxpool = nn.MaxPool2d(kernel_size=3, stride=1, padding=1)
		# self.avgpool = nn.AvgPool2d(kernel_size=3, stride=1, padding=1)

		self.conv1 = nn.Conv2d(nin, 1, kernel_size=1, stride=1, padding=0, groups=1)
		self.bn1 = nn.BatchNorm2d(1, momentum=0.9)

	def forward(self, x):
		out = self.maxpool(x)

		out = self.conv1(out)
		out = self.bn1(out)


		out = out.expand(x.shape[0], x.shape[1], x.shape[2], x.shape[3])

		out = torch.mul(out, x)

		return out
