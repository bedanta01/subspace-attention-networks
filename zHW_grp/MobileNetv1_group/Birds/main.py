import os
import shutil
import time
import warnings

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torchvision.models as models

from mobilenetv1 import mobilenetv1
from tensorboardX import SummaryWriter

warnings.filterwarnings("ignore", "(Possibly )?corrupt EXIF data", UserWarning)

root_folder = '/raid/cs17mtech11009/datasets/BirdDataset'
num_classes = 200

_folder = 'logs-1-0'

writer1 = SummaryWriter(_folder+'/exp-1')
writer2 = SummaryWriter(_folder+'/exp-2')
writer3 = SummaryWriter(_folder+'/exp-3')
writer4 = SummaryWriter(_folder+'/exp-4')
writer5 = SummaryWriter(_folder+'/exp-5')

batch_size = 128*1
learning_rate = 0.1
n_epochs = 130
iteration = 0
best_prec1 = 0

os.environ["CUDA_VISIBLE_DEVICES"]="0"

def main():
	global best_prec1
	
	traindir = os.path.join(root_folder, 'train')
	valdir = os.path.join(root_folder, 'test')
	normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
										 std=[0.229, 0.224, 0.225])

	train_loader = torch.utils.data.DataLoader(
				datasets.ImageFolder(traindir, transforms.Compose([
				transforms.RandomResizedCrop(224),
				transforms.RandomHorizontalFlip(),
				transforms.ToTensor(),
				normalize,
			])),
			batch_size=batch_size, shuffle=True,
			num_workers=90, pin_memory=True)

	val_loader = torch.utils.data.DataLoader(
				datasets.ImageFolder(valdir, transforms.Compose([
				transforms.Resize(256),
				transforms.CenterCrop(224),
				transforms.ToTensor(),
				normalize,
			])),
			batch_size=batch_size, shuffle=False,
			num_workers=90, pin_memory=True)
	
	cudnn.benchmark = True

	model = mobilenetv1(num_classes)
	# model.cuda()
	print(model)
	model = nn.DataParallel(model).cuda()
	
	criterion = nn.CrossEntropyLoss().cuda()

	optimizer = torch.optim.SGD(model.parameters(), learning_rate,
								momentum=0.9,
								weight_decay=1e-4)
	# optimizer = torch.optim.RMSprop(model.parameters(), lr=learning_rate, alpha=0.99, eps=1e-08, weight_decay=1e-4, momentum=0.9, centered=False)

	starttime = time.time()
	for epoch in range(1, n_epochs+1):
		adjust_learning_rate(optimizer, epoch)
		train_acc, train_top1_avg, train_top5_avg, train_losses_avg = train(train_loader, model, criterion, optimizer, epoch)
		test_acc, test_top1_avg, test_top5_avg, test_losses_avg = validate(val_loader, model, criterion)

		writer1.add_scalar("Training loss", train_losses_avg, epoch)
		writer2.add_scalar("Testing loss", test_losses_avg, epoch)
		writer3.add_scalar("Top1 accuracy", test_top1_avg, epoch)
		writer4.add_scalar("Top5 accuracy", test_top5_avg, epoch)

		starttime = time.time() - starttime

		print('Ep: {}   EpTime: {:.4f}  Trainloss: {:.4f}   Testloss: {:.4f}  TrainAcc: {:.2f}   Top1: {:.4f}    Top5: {:.4f}  lr: {:.6f}'.format(epoch, starttime,train_losses_avg, test_losses_avg, train_acc ,  test_top1_avg, test_top5_avg, optimizer.param_groups[0]['lr']))

		is_best = test_top1_avg > best_prec1
		best_prec1 = max(test_top1_avg, best_prec1)
		save_checkpoint({
			'epoch': epoch + 1,
			'state_dict': model.state_dict(),
			'best_prec1': best_prec1,
			'optimizer' : optimizer.state_dict(),
		}, is_best)

		starttime = time.time()

def train(train_loader, model, criterion, optimizer, epoch):
	losses = AverageMeter()
	top1 = AverageMeter()
	top5 = AverageMeter()
	batch_time = AverageMeter()
	data_time = AverageMeter()
	
	model.train()
	correct = 0
	total = 0
	end = time.time()
	
	for i, (input, target) in enumerate(train_loader):
		data_time.update(time.time() - end)

		target = target.cuda(async=True)
		input = input.cuda()

		output = model(input)
		loss = criterion(output, target)
		_, pred = torch.max(output, 1)

		# torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
		prec1, prec5 = accuracy(output.data, target, topk=(1, 5))
		losses.update(loss.item(), input.size(0))
		top1.update(prec1.item(), input.size(0))
		top5.update(prec5.item(), input.size(0))

		# compute gradient and do SGD step
		optimizer.zero_grad()
		loss.backward()
		optimizer.step()

		correct += (pred == target).sum().item()
		total += target.size(0)
		
		batch_time.update(time.time() - end)
		end = time.time()

		# if i % 10 == 0:
		# 	print('Epoch: [{0}][{1}/{2}]\t'
		# 		  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
		# 		  'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
		# 		  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
		# 		  'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
		# 		  'Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
		# 		   epoch, i, len(train_loader), batch_time=batch_time,
		# 		   data_time=data_time, loss=losses, top1=top1, top5=top5))

	train_acc = 100.*(correct/total)
	return train_acc, top1.avg, top5.avg, losses.avg


def validate(val_loader, model, criterion):
	losses = AverageMeter()
	top1 = AverageMeter()
	top5 = AverageMeter()
	batch_time = AverageMeter()

	model.eval()
	correct = 0
	total = 0
	global iteration

	end = time.time()
	for i, (input, target) in enumerate(val_loader):
		iteration = iteration + 1

		target = target.cuda(async=True)
		input = input.cuda()

		# compute output
		output = model(input)
		loss = criterion(output, target)
		_, pred = torch.max(output, 1)

		prec1, prec5 = accuracy(output.data, target, topk=(1, 5))
		losses.update(loss.item(), input.size(0))
		top1.update(prec1.item(), input.size(0))
		top5.update(prec5.item(), input.size(0))

		batch_time.update(time.time() - end)
		end = time.time()

		writer5.add_scalar("Batchwise Testloss", loss.item(), iteration)

		correct += (pred == target).sum().item()
		total += target.size(0)
		
		# if i % 10 == 0:
		# 	print('Test: [{0}/{1}]\t'
		# 		  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
		# 		  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
		# 		  'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
		# 		  'Prec@5 {top5.val:.3f} ({top5.avg:.3f})'.format(
		# 		   i, len(val_loader), batch_time=batch_time, loss=losses,
		# 		   top1=top1, top5=top5))

	# print(' * Prec@1 {top1.avg:.3f} Prec@5 {top5.avg:.3f}'
	# 	  .format(top1=top1, top5=top5))
	
	val_acc = 100.*(correct/total)
	# print(correct, total)
	return val_acc, top1.avg, top5.avg, losses.avg

def adjust_learning_rate(optimizer, epoch):
	"""Sets the learning rate to the initial LR decayed by 1/10 every 30 epochs"""
	lr = learning_rate * (0.1 ** (epoch // 30))
	for param_group in optimizer.param_groups:
		param_group['lr'] = lr

def save_checkpoint(state, is_best, filename='checkpoint.pth.tar'):
	torch.save(state, filename)
	if is_best:
		print("Checkpoint saved!!!")
		shutil.copyfile(filename, 'model_best.pth.tar')

class AverageMeter(object):
	def __init__(self):
		self.reset()

	def reset(self):
		self.val = 0
		self.avg = 0
		self.sum = 0
		self.count = 0

	def update(self, val, n=1):
		self.val = val
		self.sum += val * n
		self.count += n
		self.avg = self.sum / self.count

def accuracy(output, target, topk=(1,)):
	"""Computes the precision@k for the specified values of k"""
	maxk = max(topk)
	batch_size = target.size(0)

	_, pred = output.topk(maxk, 1, True, True)
	pred = pred.t()
	correct = pred.eq(target.view(1, -1).expand_as(pred))

	res = []
	for k in topk:
		correct_k = correct[:k].view(-1).float().sum(0)
		res.append(correct_k.mul_(100.0 / batch_size))
	return res

if __name__ == '__main__':
	main()