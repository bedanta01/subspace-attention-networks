import torch
import torch.nn as nn


class mobilenetv1(nn.Module):
	def __init__(self, num_classes):
		super(mobilenetv1, self).__init__()

		def conv_bn(inp, oup, stride):
			return nn.Sequential(
				nn.Conv2d(inp, oup, 3, stride, 1, bias=False),
				nn.BatchNorm2d(oup),
				nn.ReLU(inplace=True)
			)

		def conv_dw(inp, oup, stride, groups):
			return nn.Sequential(
				nn.Conv2d(inp, inp, 3, stride, 1, groups=groups, bias=False),
				nn.BatchNorm2d(inp),
				nn.ReLU(inplace=True),
	
				nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
				nn.BatchNorm2d(oup),
				nn.ReLU(inplace=True),
			)
			
		self.g = 4

		self.model = nn.Sequential(
			conv_bn(  3,  32, 2), 
			conv_dw( 32,  64, 1, self.g*1),
			conv_dw( 64, 128, 2, self.g*2),
			conv_dw(128, 128, 1, self.g*4),
			conv_dw(128, 256, 2, self.g*4),
			conv_dw(256, 256, 1, self.g*8),
			conv_dw(256, 512, 2, self.g*8),
			conv_dw(512, 512, 1, self.g*16),
			conv_dw(512, 512, 1, self.g*16),
			conv_dw(512, 512, 1, self.g*16),
			conv_dw(512, 512, 1, self.g*16),
			conv_dw(512, 512, 1, self.g*16),
			conv_dw(512, 1024, 2, self.g*16),
			conv_dw(1024, 1024, 1, self.g*32),
			nn.AvgPool2d(7),
		)
		self.fc = nn.Linear(1024, num_classes)

	def forward(self, x):
		x = self.model(x)
		x = x.view(-1, 1024)
		x = self.fc(x)
		return x
		